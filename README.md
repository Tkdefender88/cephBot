# CephBot
  
  A simple discord bot made in Go. This bot is still currently in development and does not have a public join link yet (come back later if you're interested in that). If you desperately want the bot right now you can clone it and host it locally.
  
## Features:
  * Meme generator using common meme templates
  * Urban dictionary lookup
  * xkcd comics
  * leet speak
  * ping pong
  * reward stars (reward users for good behavior)
  * Big emojis

## Things I'll maybe add sometime in the Future:

  * user info
  * twitter updates
  * possibly some DND releated commands
  
## Built with
  
  [Discordgo](https://github.com/bwmarrin/discordgo) - The discord API library for go
  
## Special Thanks to:

  [Strum355](https://github.com/Strum355/2Bot-Discord-Bot) For allowing me to look at the source code for his '2bot' for learning the discord API
  
  [The discord gophers server](https://discord.gg/5uKc5Tx) For all their help in my fumblings in the dark with this new language and API
  
